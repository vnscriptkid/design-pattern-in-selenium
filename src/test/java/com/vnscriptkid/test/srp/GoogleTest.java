package com.vnscriptkid.test.srp;

import com.vnscriptkid.srp.main.MainPage;
import com.vnscriptkid.srp.result.ResultPage;
import com.vnscriptkid.test.BaseTest;
import org.testng.Assert;
import org.testng.annotations.BeforeTest;
import org.testng.annotations.DataProvider;
import org.testng.annotations.Test;

public class GoogleTest extends BaseTest {
    private MainPage mainPage;
    private ResultPage resultPage;

    @BeforeTest
    public void start() {
        mainPage = new MainPage(driver);
        resultPage = new ResultPage(driver);
    }

    @Test(dataProvider = "testData")
    public void runMainFlow(String searchTerm, int itemIndex) {
        mainPage.goTo();
        Assert.assertTrue(mainPage.getSearchInput().isDisplayed());

        // main page
        mainPage.getSearchInput().enter(searchTerm);
        Assert.assertTrue(mainPage.getSuggestionDropdown().isDisplayed());

        mainPage.getSuggestionDropdown().selectItem(itemIndex);
        Assert.assertTrue(resultPage.getSearchNavigation().isDisplayed());

        // result page
        resultPage.getSearchInput().enterCharByChar(searchTerm);
        Assert.assertTrue(resultPage.getSuggestionDropdown().isDisplayed());

        resultPage.getSuggestionDropdown().selectItem(itemIndex);
        Assert.assertTrue(resultPage.getSearchNavigation().isDisplayed());

        resultPage.getSearchNavigation().selectVideoLink();
        System.out.println(resultPage.getResultStats().getStatsInText());;
    }

    @DataProvider(name = "testData")
    private Object[][] feedData() {
        return new Object[][] {
                { "coffee", 3 },
                { "juice", 4 }
        };
    }
}
