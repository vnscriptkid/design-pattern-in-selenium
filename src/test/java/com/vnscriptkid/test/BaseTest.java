package com.vnscriptkid.test;

import com.google.common.util.concurrent.Uninterruptibles;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.testng.annotations.AfterTest;
import org.testng.annotations.BeforeTest;

import java.util.concurrent.TimeUnit;

public class BaseTest {
    protected WebDriver driver;

    @BeforeTest
    public void baseStart() {
        System.setProperty("webdriver.chrome.driver", "D:\\Downloads\\Katalon_Studio_Windows_64-7.7.2\\configuration\\resources\\drivers\\chromedriver_win32\\chromedriver.exe");
        this.driver = new ChromeDriver();
    }

    @AfterTest
    public void baseDone() {
        Uninterruptibles.sleepUninterruptibly(3, TimeUnit.SECONDS);
        this.driver.quit();
    }
}
