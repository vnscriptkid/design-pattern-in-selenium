package com.vnscriptkid.srp.main;

import com.vnscriptkid.srp.common.SearchInput;
import com.vnscriptkid.srp.common.SuggestionDropdown;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.support.PageFactory;

public class MainPage {
    private SearchInput searchInput;
    private SuggestionDropdown suggestionDropdown;
    private WebDriver driver;

    public MainPage(final WebDriver driver) {
        this.driver = driver;
        searchInput = PageFactory.initElements(driver, SearchInput.class);
        suggestionDropdown = PageFactory.initElements(driver, SuggestionDropdown.class);
    }

    public void goTo() {
        driver.navigate().to("https://www.google.com/");
    }

    public SearchInput getSearchInput() {
        return searchInput;
    }

    public SuggestionDropdown getSuggestionDropdown() {
        return suggestionDropdown;
    }
}
