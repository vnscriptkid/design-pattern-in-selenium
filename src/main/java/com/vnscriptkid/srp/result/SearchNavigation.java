package com.vnscriptkid.srp.result;

import com.vnscriptkid.srp.common.AbtractComponent;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;

public class SearchNavigation extends AbtractComponent {

    @FindBy(id = "hdtb")
    WebElement navBar;
    @FindBy(linkText = "Hình ảnh")
    WebElement imagesLink;

    @FindBy(linkText = "Tất cả")
    WebElement allLink;

    @FindBy(linkText = "Video")
    WebElement videoLink;

    public SearchNavigation(WebDriver driver) {
        super(driver);
    }

    public void selectImagesLink() {
        imagesLink.click();
    }

    public void selectAllLink() {
        allLink.click();
    }

    public void selectVideoLink() {
        videoLink.click();
    }

    @Override
    public boolean isDisplayed() {
        return wait.until(driver -> navBar.isDisplayed());
    }

}
