package com.vnscriptkid.srp.result;

import com.vnscriptkid.srp.common.SearchInput;
import com.vnscriptkid.srp.common.SuggestionDropdown;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.support.PageFactory;

public class ResultPage {
    private SearchInput searchInput;
    private SearchNavigation searchNavigation;
    private ResultStats resultStats;
    private SuggestionDropdown suggestionDropdown;
    private WebDriver driver;

    public ResultPage(final WebDriver driver) {
        this.driver = driver;
        suggestionDropdown = PageFactory.initElements(driver, SuggestionDropdown.class);
        searchInput = PageFactory.initElements(driver, SearchInput.class);
        searchNavigation = PageFactory.initElements(driver, SearchNavigation.class);
        resultStats = PageFactory.initElements(driver, ResultStats.class);
    }

    public SearchInput getSearchInput() {
        return searchInput;
    }

    public ResultStats getResultStats() {
        return resultStats;
    }

    public SearchNavigation getSearchNavigation() {
        return searchNavigation;
    }

    public SuggestionDropdown getSuggestionDropdown() {
        return suggestionDropdown;
    }
}
