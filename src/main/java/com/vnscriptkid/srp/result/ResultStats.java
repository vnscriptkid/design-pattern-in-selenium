package com.vnscriptkid.srp.result;

import com.vnscriptkid.srp.common.AbtractComponent;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;

public class ResultStats extends AbtractComponent {

    @FindBy(id = "result-stats")
    WebElement stats;

    public ResultStats(final WebDriver driver) {
        super(driver);
    }

    public String getStatsInText() {
        return stats.getText();
    }

    @Override
    public boolean isDisplayed() {
        return wait.until(driver -> stats.isDisplayed());
    }
}
