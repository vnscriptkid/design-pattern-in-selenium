package com.vnscriptkid.srp.common;

import com.google.common.util.concurrent.Uninterruptibles;
import com.vnscriptkid.srp.common.AbtractComponent;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;

import java.util.concurrent.TimeUnit;

public class SearchInput extends AbtractComponent {

    @FindBy(name = "q")
    private WebElement input;

    public SearchInput(final WebDriver driver) {
        super(driver);
    }

    public void enter(String text) {
        input.clear();
        input.sendKeys(text);
    }

    public void enterCharByChar(String text) {
        input.clear();
        for (Character c : text.toCharArray()) {
            input.sendKeys(c.toString());
            Uninterruptibles.sleepUninterruptibly(200, TimeUnit.MILLISECONDS);
        }
    }

    @Override
    public boolean isDisplayed() {
        return wait.until(driver -> input.isDisplayed());
    }
}
