package com.vnscriptkid.srp.common;

import com.vnscriptkid.srp.common.AbtractComponent;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;

import java.util.List;

public class SuggestionDropdown extends AbtractComponent {

    @FindBy(xpath = "//*[@role='listbox']//li[@role='presentation' and @data-view-type='1']//*[contains(@class, 'sbtc')]")
    private List<WebElement> items;

    public SuggestionDropdown(final WebDriver driver) {
        super(driver);
    }

    public void selectItem(int nth) {
        items.get(nth - 1).click();
    }

    @Override
    public boolean isDisplayed() {
        return this.wait.until(driver -> items.size() > 5);
    }
}
